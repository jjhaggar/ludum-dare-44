﻿using UnityEngine;
using System.Collections;
using System;
using System.Collections.Generic;
using System.Linq;
using Rewired;

namespace Assets.Scripts
{
	public static class Controls
	{
        public static int playerId0 = 0;

        public static Rewired.Player _player;

        public static void ActivateRewired()
        {
            _player = ReInput.players.GetPlayer(playerId0);
        }

		public static int IsKeyPressed()
		{
			return Rewired.ReInput.controllers.Keyboard.PollForFirstKey().elementIndex;
        }

        public static bool IsLeftPressed(int id)
        {
            //return Input.GetAxisRaw("Horizontal Keyboard") < 0.0f || Input.GetAxisRaw("Horizontal Joystick") < 0.0f || _isLeftPressedAutomatically;
            return _player.GetAxisRaw("Move Horizontal") < 0.0f || _player.GetAxis("Move Horizontal Joystick") < -0.5f;
		}
		public static bool IsRightPressed(int id)
		{
			//return Input.GetAxisRaw("Horizontal Keyboard") > 0.0f || Input.GetAxisRaw("Horizontal Joystick") > 0.0f;
			return _player.GetAxisRaw("Move Horizontal") > 0.0f || _player.GetAxis("Move Horizontal Joystick") > 0.5f;
		}

		public static bool IsDownPressed(int id)
		{
			//return Input.GetAxisRaw("Vertical Keyboard") < 0.0f || Input.GetAxisRaw("Vertical Joystick") < 0.0f || _isDownPressedAutomatically;
			return _player.GetAxisRaw("Move Vertical") < 0.0f || _player.GetAxis("Move Vertical Joystick") < -0.5f;
		}

		public static bool IsUpPressed(int id)
		{
			//return Input.GetAxisRaw("Vertical Keyboard") > 0.0f || Input.GetAxisRaw("Vertical Joystick") > 0.0f;
			return _player.GetAxisRaw("Move Vertical") > 0.0f || _player.GetAxis("Move Vertical Joystick") > 0.5f;
		}

		public static bool IsEnterPressed(int id)
		{
			//return Input.GetButton("Start Button");
			return _player.GetButton("Pause");
		}

		public static bool IsEscapePressed(int id)
		{
			//return Input.GetKeyDown(KeyCode.Escape);
			return _player.GetButton("Escape");
		}

		public static bool IsFirePressed(int id)
		{
			return IsSimpleFirePressed(id);
        }

        public static bool IsSecondWeaponPressed(int id)
        {
            return _player.GetButton("Second Weapon");
        }

        public static bool IsSuperPowerPressed(int id)
        {
            return _player.GetButton("Super Power");
        }

        private static bool IsSimpleFirePressed(int id)
		{
			//return Input.GetButton("Fire") || Input.GetButton("Fire Alternate Button");
			return _player.GetButton("Fire");
        }

		public static bool IsJumpPressed(int id)
		{
			 return IsSimpleJumpPressed(id);
		}

		private static bool IsSimpleJumpPressed(int id)
		{
			//return Input.GetButton("Jump") || Input.GetButton("Jump Alternate Button");
			return _player.GetButton("Jump");
        }

		public static bool IsChangeWeaponLeftPressed(int id)
		{
			//return Input.GetButton("Change Weapon Left");
			return _player.GetButton("Change Weapon Left");
		}

		public static bool IsChangeWeaponRightPressed(int id)
		{
			//return Input.GetButton("Change Weapon Right");
			return _player.GetButton("Change Weapon Right");
		}

        public static bool IsDashPressed(int id)
        {
            return _player.GetButton("Dash");
        }

        //only directions and normal buttons. no enter or escape
        public static bool IsDirOrNormButtonPressed(int id)
        {
			//return Input.GetAxisRaw("Horizontal Keyboard") < 0.0f || Input.GetAxisRaw("Horizontal Joystick") < 0.0f || IsRightPressed() || IsDownPressed() || IsUpPressed()
			//  || IsFirePressed() || IsJumpPressed() || IsChangeWeaponLeftPressed() || IsChangeWeaponRightPressed();
			return _player.GetAxisRaw("Horizontal Keyboard") < 0.0f || _player.GetAxisRaw("Horizontal Joystick") < 0.0f || IsRightPressed(id) || IsDownPressed(id) || IsUpPressed(id)
			    || IsFirePressed(id) || IsJumpPressed(id) || IsChangeWeaponLeftPressed(id) || IsChangeWeaponRightPressed(id);
		}

		//enter and escape included and normal buttons. no directions
		public static bool IsSkipButtonPressed(int id)
        {
            return IsEnterPressed(id) || IsEscapePressed(id)
                || IsSimpleFirePressed(id) || IsSimpleJumpPressed(id) || IsChangeWeaponLeftPressed(id) || IsChangeWeaponRightPressed(id);
        }

        public static bool IsAcceptButtonPressed(int id)
        {
                return IsEnterPressed(id) || IsSimpleJumpPressed(id);
        }

        public static bool IsCancelButtonPressed(int id)
        {
                return IsEscapePressed(id) || IsSecondWeaponPressed(id);
            
        }

        public static bool IsKusarigamaPressed(int id)
        {
            return _player.GetButton("Kusarigama");
        }
    }
}
