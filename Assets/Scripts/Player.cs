﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using UnityEngine.UI;

namespace Assets.Scripts
{
    class Player
    {
        private tk2dSpriteAnimator _animator;

        // Input buffer
        public bool LeftPressed { get; set; }
        public bool RightPressed { get; set; }
        public bool RightToBeConsumed { get; set; }
        public bool LeftToBeconsumed { get; set; }

        public bool UpPressed { get; set; }
        public bool DownPressed { get; set; }
        public bool UpToBeConsumed { get; set; }
        public bool DownToBeConsumed { get; set; }

        public int GridPositionX { get; set; }
        public int GridPositionY { get; set; }

        private Text _charLife;
        private Text _charMoney;

        private int _life = 21;
        public int Money { get; set; }

        public bool Dead { get; set; }

        private bool _neverMadeSound = true;

        public Player(tk2dSpriteAnimator player, Text charLife, Text charMoney, int initialPosGridX, int initialPosGridY)
        {
            _animator = player;
            GridPositionX = initialPosGridX;
            GridPositionY = initialPosGridY;

            _charLife = charLife;
            _charMoney = charMoney;

            UpdateUI();
        }

        private void UpdateUI()
        {
            _charLife.text = "HP = " + _life;
            _charMoney.text = "Money = " + Money;
        }

        public void Update(float deltaTime, Money[] moneyList, Spike[] spikeList, Obstacle[] obstacleList)
        {
            InputManagement();

            Movement(obstacleList);

            Collisions(moneyList, spikeList);
        }

        private void Collisions(Money[] moneyList, Spike[] spikeList)
        {
            bool moneySounded = false;
            // money
            for (int i = 0; i < moneyList.Length; i++)
            {
                if (moneyList[i].GridPositionX == GridPositionX && moneyList[i].GridPositionY == GridPositionY)
                {
                    if (!moneyList[i].Obtained)
                    {
                        moneyList[i].Obtained = true;
                        moneyList[i].Animator.gameObject.SetActive(false);

                        Money += 1000;
                        _life += 1; //because of bug
                        UpdateUI();

                        moneySounded = true;

                        MainGameScript.sounds.PlayOneShot((AudioClip)Resources.Load("Coins"));
                    }
                }
            }

            // spikes
            for (int i = 0; i < spikeList.Length; i++)
            {
                if (spikeList[i].GridPositionX == GridPositionX && spikeList[i].GridPositionY == GridPositionY)
                {
                    if (!spikeList[i].Deactivated)
                    {
                        spikeList[i].Deactivated = true;
                        spikeList[i].Animator.gameObject.SetActive(false);

                        _life -= 1;

                        if (_life <= 0)
                        {
                            MainGameScript.sounds.PlayOneShot((AudioClip)Resources.Load("Death"));
                            Dead = true;
                            _animator.Play("PlayerDead");
                        }
                        else
                        {
                            if (_neverMadeSound)
                            {
                                //solve bug beginning
                                _neverMadeSound = false;
                            }
                            else
                            {
                                if (!moneySounded)
                                    MainGameScript.sounds.PlayOneShot((AudioClip)Resources.Load("Damage"));
                            }
                        }
                        UpdateUI();
                    }
                }
            }
        }

        private void Movement(Obstacle[] obstacleList)
        {
            if (Dead)
                return;

            if (LeftToBeconsumed)
            {
                LeftToBeconsumed = false;
                if (GridPositionX > 0)
                {
                    for (int i = 0; i < obstacleList.Length; i++)
                    {
                        if (GridPositionX - 1 == obstacleList[i].GridPositionX 
                            && GridPositionY == obstacleList[i].GridPositionY)
                        {
                            MainGameScript.sounds.PlayOneShot((AudioClip)Resources.Load("Obstacles"));
                            return;
                        }
                    }

                    PositionX -= 30f;
                    GridPositionX -= 1;
                }
            }
            else if (RightToBeConsumed)
            {
                RightToBeConsumed = false;
                if (GridPositionX < MainGameScript.GRID_WIDTH - 1)
                {
                    for (int i = 0; i < obstacleList.Length; i++)
                    {
                        if (GridPositionX + 1 == obstacleList[i].GridPositionX
                            && GridPositionY == obstacleList[i].GridPositionY)
                        {
                            MainGameScript.sounds.PlayOneShot((AudioClip)Resources.Load("Obstacles"));
                            return;
                        }
                    }

                    PositionX += 30f;
                    GridPositionX += 1;
                }
            }
            else if (UpToBeConsumed)
            {
                UpToBeConsumed = false;
                if (GridPositionY < MainGameScript.GRID_HEIGHT - 1)
                {
                    for (int i = 0; i < obstacleList.Length; i++)
                    {
                        if (GridPositionX == obstacleList[i].GridPositionX
                            && GridPositionY + 1 == obstacleList[i].GridPositionY)
                        {
                            MainGameScript.sounds.PlayOneShot((AudioClip)Resources.Load("Obstacles"));
                            return;
                        }
                    }

                    PositionY += 30f;
                    PositionZ += 30f;
                    GridPositionY += 1;
                }
            }
            else if (DownToBeConsumed)
            {
                DownToBeConsumed = false;
                if (GridPositionY > 0)
                {
                    for (int i = 0; i < obstacleList.Length; i++)
                    {
                        if (GridPositionX == obstacleList[i].GridPositionX
                            && GridPositionY - 1 == obstacleList[i].GridPositionY)
                        {
                            MainGameScript.sounds.PlayOneShot((AudioClip)Resources.Load("Obstacles"));
                            return;
                        }
                    }

                    PositionY -= 30f;
                    PositionZ -= 30f;
                    GridPositionY -= 1;
                }
            }
        }

        private void InputManagement()
        {
            if (Controls.IsLeftPressed(0))
            {
                if (!LeftPressed)
                    LeftToBeconsumed = true;
                LeftPressed = true;
            }
            else
            {
                LeftPressed = false;
            }
            if (Controls.IsRightPressed(0))
            {
                if (!RightPressed)
                    RightToBeConsumed = true;
                RightPressed = true;
            }
            else
            {
                RightPressed = false;
            }

            if (Controls.IsUpPressed(0))
            {
                if (!UpPressed)
                    UpToBeConsumed = true;
                UpPressed = true;
            }
            else
            {
                UpPressed = false;
            }
            if (Controls.IsDownPressed(0))
            {
                if (!DownPressed)
                    DownToBeConsumed = true;
                DownPressed = true;
            }
            else
            {
                DownPressed = false;
            }
        }

        public float PositionX
        {
            get
            {
                return _animator.transform.position.x;
            }
            set
            {
                _animator.transform.position = new Vector3(value, _animator.transform.position.y, _animator.transform.position.z);
            }
        }

        public float PositionY
        {
            get
            {
                return _animator.transform.position.y;
            }
            set
            {
                _animator.transform.position = new Vector3(_animator.transform.position.x, value, _animator.transform.position.z);
            }
        }

        public float PositionZ
        {
            get
            {
                return _animator.transform.position.z;
            }
            set
            {
                _animator.transform.position = new Vector3(_animator.transform.position.x, _animator.transform.position.y, value);
            }
        }
    }
}
