﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Assets.Scripts
{
    class Money
    {
        public int GridPositionX { get; set; }
        public int GridPositionY { get; set; }

        public tk2dSpriteAnimator Animator { get; set; }

        public bool Obtained { get; set; }

        public Money(tk2dSpriteAnimator animator, int initialGridX, int initialGridY)
        {
            Animator = animator;

            GridPositionX = initialGridX;
            GridPositionY = initialGridY;
        }
    }
}
