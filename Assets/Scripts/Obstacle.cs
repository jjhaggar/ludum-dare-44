﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Assets.Scripts
{
    class Obstacle
    {
        public int GridPositionX { get; set; }
        public int GridPositionY { get; set; }

        public tk2dSpriteAnimator Animator { get; set; }

        public Obstacle(tk2dSpriteAnimator animator, int initialGridX, int initialGridY)
        {
            Animator = animator;

            GridPositionX = initialGridX;
            GridPositionY = initialGridY;
        }
    }
}
