﻿using System;
using System.Collections;
using System.Collections.Generic;
using Assets.Scripts;
using UnityEngine;
using UnityEngine.UI;

public class MainGameScript : MonoBehaviour
{
    private float _stateTime;
    private float _stateTimeDead;

    private tk2dSpriteAnimator _gridAnimator;
    private tk2dSpriteAnimator _pawnAnimator;
    private tk2dSpriteAnimator _spikeAnimator;
    private tk2dSpriteAnimator _moneyAnimator;
    private tk2dSpriteAnimator _enemyAnimator;
    private tk2dSpriteAnimator _gameOverScreen;
    private tk2dSpriteAnimator _obstacleAnimator;


    private Text _charMoney;
    private Text _charLife;
    private Text _gameOverText;
    private Text _gameOverMoney;

    private Spike[] _spikes;

    private Player _player;
    private Money[] _money;
    private Obstacle[] _obstacles;

    private int _initialPositionPlayerX = 0;
    private int _initialPositionPlayerY = 3;

    private Vector2[] _initialPositionMoney = new Vector2[] {
        new Vector2(2, 6),
        new Vector2(7, 6),
        new Vector2(13, 6),
        new Vector2(15, 6),
        new Vector2(13, 5),
        new Vector2(15, 5),
        new Vector2(15, 4),
        new Vector2(13, 3),
        new Vector2(5, 2),
        new Vector2(14, 2),
        new Vector2(15, 2),
        new Vector2(1, 1),
        new Vector2(2, 1),
        new Vector2(10, 1),
        new Vector2(6, 0) };

    private Vector2[] _initialPositionObstacle = new Vector2[] {
        new Vector2(9, 6),
        new Vector2(10, 6),
        new Vector2(4, 5),
        new Vector2(6, 4),
        new Vector2(3, 3),
        new Vector2(3, 2),
        new Vector2(6, 1),
        new Vector2(6, 2),
        new Vector2(7, 2),
        new Vector2(8, 2),
        new Vector2(7, 2)};

    public const int GRID_WIDTH = 16;
    public const int GRID_HEIGHT = 9;
    private AudioSource audioSource;
    private bool _initialized;
    private GameObject _windowGameOver;
    public static AudioSource sounds;
    private bool _kingSounded;

    void Start()
    {
        _gridAnimator = GameObject.FindGameObjectWithTag("Grid").GetComponent<tk2dSpriteAnimator>();
        _pawnAnimator = GameObject.FindGameObjectWithTag("Pawn").GetComponent<tk2dSpriteAnimator>();
        _spikeAnimator = GameObject.FindGameObjectWithTag("Spike").GetComponent<tk2dSpriteAnimator>();
        _moneyAnimator = GameObject.FindGameObjectWithTag("Money").GetComponent<tk2dSpriteAnimator>();
        _enemyAnimator = GameObject.FindGameObjectWithTag("Enemy").GetComponent<tk2dSpriteAnimator>();
        _obstacleAnimator = GameObject.FindGameObjectWithTag("Obstacle").GetComponent<tk2dSpriteAnimator>();

        _charLife = GameObject.FindGameObjectWithTag("CharLife").GetComponent<Text>();
        _charMoney = GameObject.FindGameObjectWithTag("CharMoney").GetComponent<Text>();

        _gameOverText = GameObject.Find("GameOverText").GetComponent<Text>();
        _gameOverMoney = GameObject.Find("MoneyEnd").GetComponent<Text>();
        _gameOverScreen = GameObject.Find("GameOver").GetComponent<tk2dSpriteAnimator>();
        _gameOverText.gameObject.SetActive(false);
        _gameOverMoney.gameObject.SetActive(false);
        _gameOverScreen.gameObject.SetActive(false);

        Controls.ActivateRewired();

        _player = new Player(_pawnAnimator, _charLife, _charMoney, _initialPositionPlayerX, _initialPositionPlayerY);

        CreateMoney();

        CreateObstacle();

        CreateSpikes();

        _spikeAnimator.gameObject.SetActive(false);
        _moneyAnimator.gameObject.SetActive(false);
        _enemyAnimator.gameObject.SetActive(false);
        _obstacleAnimator.gameObject.SetActive(false);

        _charMoney.gameObject.SetActive(false);
        _charLife.gameObject.SetActive(false);

        _windowGameOver = GameObject.Find("WindowGameOver");
        _windowGameOver.SetActive(false);

        audioSource = GameObject.Find("Music1").GetComponent<AudioSource>();
        audioSource.clip = (AudioClip)Resources.Load("pawn");
        audioSource.loop = true;
        audioSource.Play();

        sounds = GameObject.Find("Sounds").GetComponent<AudioSource>();
    }

    void Update()
    {
        float delta = Time.deltaTime;

        _stateTime += delta;

        if (_stateTime > 5f && !_initialized)
        {
            _initialized = true;

            _charMoney.gameObject.SetActive(true);
            _charLife.gameObject.SetActive(true);

            GameObject.Find("IntroScreen").SetActive(false);
        }


        if (_stateTime > 5f)
        {
            _player.Update(delta, _money, _spikes, _obstacles);

            if (Input.GetKey(KeyCode.Escape))
                Application.Quit();

            if (_player.Dead)
            {
                audioSource.Stop();
                _stateTimeDead += delta;
                if (_stateTimeDead > 5f)
                {
                    EndGame();

                    if (_stateTimeDead > 10f)
                        Application.Quit();
                }
            }
        }
    }

    private void EndGame()
    {
        _windowGameOver.SetActive(true);

        _gameOverText.gameObject.SetActive(true);
        _gameOverMoney.gameObject.SetActive(true);
        _gameOverScreen.gameObject.SetActive(true);
        _gameOverMoney.text = "Money = " + _player.Money;

        if (!_kingSounded)
        {
            _kingSounded = true;
            MainGameScript.sounds.PlayOneShot((AudioClip)Resources.Load("King_Evil"));
        }
    }

    private void CreateObstacle()
    {
        _obstacles = new Obstacle[_initialPositionObstacle.Length];

        for (int i = 0; i < _initialPositionObstacle.Length; i++)
        {
            tk2dSpriteAnimator animator = new tk2dSpriteAnimator();
            animator = MonoBehaviour.Instantiate<tk2dSpriteAnimator>(_obstacleAnimator);
            animator.transform.position = new Vector3(
                _obstacleAnimator.transform.position.x + _initialPositionObstacle[i].x * 30f,
                _obstacleAnimator.transform.position.y + _initialPositionObstacle[i].y * 30f,
                _obstacleAnimator.transform.position.z + _initialPositionObstacle[i].y * 30f);

            _obstacles[i] = new Obstacle(animator, (int)_initialPositionObstacle[i].x, (int)_initialPositionObstacle[i].y);
            animator.gameObject.SetActive(false);
        }
    }

    private void CreateMoney()
    {
        _money = new Money[_initialPositionMoney.Length];

        for (int i = 0; i < _initialPositionMoney.Length; i++)
        {
            tk2dSpriteAnimator animator = new tk2dSpriteAnimator();
            animator = MonoBehaviour.Instantiate<tk2dSpriteAnimator>(_moneyAnimator);
            animator.transform.position = new Vector3(
                _moneyAnimator.transform.position.x + _initialPositionMoney[i].x * 30f,
                _moneyAnimator.transform.position.y + _initialPositionMoney[i].y * 30f,
                _moneyAnimator.transform.position.z + _initialPositionMoney[i].y * 30f);

            _money[i] = new Money(animator, (int)_initialPositionMoney[i].x, (int)_initialPositionMoney[i].y);
        }

    }

    private void CreateSpikes()
    {
        int x = GRID_WIDTH;
        int y = GRID_HEIGHT;

        _spikes = new Spike[x * y];

        for (int i = 0; i < x; i++)
        {
            for (int j = 0; j < y; j++)
            {
                tk2dSpriteAnimator animator = new tk2dSpriteAnimator();
                animator = MonoBehaviour.Instantiate<tk2dSpriteAnimator>(_spikeAnimator);

                if (_player.GridPositionX == i && _player.GridPositionY == j)
                {
                    DeactivateSpikes(i, j, animator);
                }

                for (int k = 0; k < _money.Length; k++)
                {
                    if (_money[k].GridPositionX == i && _money[k].GridPositionY == j)
                    {
                        DeactivateSpikes(i, j, animator);
                    }
                }

                for (int k = 0; k < _obstacles.Length; k++)
                {
                    if (_obstacles[k].GridPositionX == i && _obstacles[k].GridPositionY == j)
                    {
                        DeactivateSpikes(i, j, animator);
                    }
                }

                animator.transform.position = new Vector3(
                    _spikeAnimator.transform.position.x + i * 30f,
                    _spikeAnimator.transform.position.y + j * 30f,
                    _spikeAnimator.transform.position.z + j * 30f);

                _spikes[i + GRID_WIDTH * j] = new Spike(animator, i, j);
            }
        }
    }

    private void DeactivateSpikes(int i, int j, tk2dSpriteAnimator animator)
    {
        _spikes[i + GRID_WIDTH * j] = new Spike(animator, i, j);
        _spikes[i + GRID_WIDTH * j].Deactivated = true;
        _spikes[i + GRID_WIDTH * j].Animator.gameObject.SetActive(false);
    }
}
