﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Assets.Scripts
{
    class Spike
    {
        public int GridPositionX { get; set; }
        public int GridPositionY { get; set; }

        public bool Deactivated { get; set; }

        public tk2dSpriteAnimator Animator { get; set; }

        public Spike(tk2dSpriteAnimator spike, int initialGridX, int initialGridY)
        {
            Animator = spike;

            GridPositionX = initialGridX;
            GridPositionY = initialGridY;
        }
    }
}
